import javax.swing.JOptionPane;

public class Prestamos{

public static void main(String arg[]){
    
    String nombre, placa, accesorios, modelo, tipo, usuario, observaciones, fechaP, fechaD, identificacion, datoLeido;
    int menuP, contadorU, contadorE, menuU, menuE;
    
    contadorU=0;
    contadorE=0;
    
    identificacion= "Sin registrar";
    nombre= "Sin registrar";
    usuario= "Sin registrar";
    fechaP= "Sin registrar";
    fechaD= "Sin registrar";
    
    Equipo datosEquipo=null;
    
    //SE TERMINA DE INICIALIZAR CADA PARAMETRO
    
    do{      
      datoLeido=JOptionPane.showInputDialog("******Prestamos******"+
                                            "\n1. Registrar Usuario"+
                                            "\n2. Modificar Usuario"+
                                            "\n3. Registrar datos de un Equipo"+
                                            "\n4. Modificar atributos de un Equipo"+
                                            "\n5. Mostrar atributos de un Equipo"+
                                            "\n6. Mostrar datos del Prestamo"+
                                            "\n7. Salir");
      
      menuP=Integer.parseInt(datoLeido);
      
      switch(menuP){
        case 1:
          
          if(contadorU==0){
          
          identificacion=JOptionPane.showInputDialog("Ingrese su identificacion: ");
                    
          nombre=JOptionPane.showInputDialog("Ingrese su nombre: ");
          
          usuario=JOptionPane.showInputDialog("Ingrese que tipo de usuario eres (Estudiante o Docente): ");
          
          fechaP=JOptionPane.showInputDialog("Ingrese la fecha en el que el prestamo se realiza: ");
          
          fechaD=JOptionPane.showInputDialog("Ingrese la fecha en el que el equipo se devuelve: ");
          
          contadorU++;          
        
        }else{
            
          datoLeido=JOptionPane.showInputDialog("Ya hay un usuario registrado, registrar uno nuevo eliminara los datos del anterior. �Quiere continuar?"+"\n1. Si"+"\n2. No");          
          menuU=Integer.parseInt(datoLeido);
          
          switch(menuU){
            case 1:
              
              identificacion=JOptionPane.showInputDialog("Ingrese su identificacion: ");
                          
              nombre=JOptionPane.showInputDialog("Ingrese su nombre: ");
              
              usuario=JOptionPane.showInputDialog("Ingrese que tipo de usuario eres (Estudiante o Docente): ");
          
              fechaP=JOptionPane.showInputDialog("Ingrese la fecha en el que el prestamo se realiza: ");
          
              fechaD=JOptionPane.showInputDialog("Ingrese la fecha en el que el equipo se devuelve: ");
          
              contadorU++;
              
              break;
              
            default:
              JOptionPane.showMessageDialog(null, "No se registrara un nuevo usuario.");
              
        //Lo anterior es el controlador de la opcion numero 1
              
        }//FIN DEL SWITCH DE USUARIO
          
        }// FIN DEL IF
          
        
          break; 
          
          case 2:
            
            if(contadorU!=0){
            
            identificacion=JOptionPane.showInputDialog("Ingrese su identificacion: ");
                     
          nombre=JOptionPane.showInputDialog("Ingrese su nombre: ");
          
          usuario=JOptionPane.showInputDialog("Ingrese que tipo de usuario eres (Estudiante o Docente): ");
          
          fechaP=JOptionPane.showInputDialog("Ingrese la fecha en el que el prestamo se realiza: ");
          
          fechaD=JOptionPane.showInputDialog("Ingrese la fecha en el que el equipo se devuelve: ");
          
          
          }else{
            
            JOptionPane.showMessageDialog(null, "No hay un usuario registrado para modificar, primero debe registrar uno.");          
          
          }
          
          //Lo anterior es el controlador de la opcion numero 2
            
          break;
          
        case 3:
          
          if(contadorE==0){
          
          placa=JOptionPane.showInputDialog("Ingrese la placa del equipo: ");
          
          accesorios=JOptionPane.showInputDialog("Ingrese los accesorios del equipo: ");
          
          observaciones=JOptionPane.showInputDialog("Ingrese el estado del equipo: ");
          
          tipo=JOptionPane.showInputDialog("Ingrese el tipo del equipo: ");
          
          modelo=JOptionPane.showInputDialog("Ingrese el modelo del equipo: ");
          
          datosEquipo=new Equipo(placa, accesorios, observaciones, tipo, modelo);
          
          contadorE++;          
        
        }else{
            
          datoLeido=JOptionPane.showInputDialog("Ya hay un equipo registrado, registrar uno nuevo eliminara los datos del anterior. �Quiere continuar?"+"\n1. Si"+"\n2. No");          
          menuE=Integer.parseInt(datoLeido);
          
          switch(menuE){
            case 1:
              
              placa=JOptionPane.showInputDialog("Ingrese la placa del equipo: ");
          
              accesorios=JOptionPane.showInputDialog("Ingrese los accesorios del equipo: ");
          
              observaciones=JOptionPane.showInputDialog("Ingrese el estado del equipo: ");
          
              tipo=JOptionPane.showInputDialog("Ingrese el tipo del equipo: ");
          
              modelo=JOptionPane.showInputDialog("Ingrese el modelo del equipo: ");       
              
              datosEquipo=new Equipo(placa, accesorios, observaciones, tipo, modelo);
          
              contadorE++;
              
              break;
              
            default:
              JOptionPane.showMessageDialog(null, "No se registrara un nuevo equipo.");
          
          }//FIN DEL SWITCH DEL MenuE
          }//FIN DEL IF Y ELSE
        
        //LO ANTERIOR ES EL CONTROLADOR DEL NUMERO 3
        
        
        break;
          
          case 4:
            
              if(contadorE!=0){
            
            placa=JOptionPane.showInputDialog("Ingrese la placa del equipo: ");
          
            accesorios=JOptionPane.showInputDialog("Ingrese los accesorios del equipo: ");
          
            observaciones=JOptionPane.showInputDialog("Ingrese el estado del equipo: ");
          
            tipo=JOptionPane.showInputDialog("Ingrese el tipo del equipo: ");
          
            modelo=JOptionPane.showInputDialog("Ingrese el modelo del equipo: ");
            
            datosEquipo=new Equipo(placa, accesorios, observaciones, tipo, modelo);
          
          }else{
            
            JOptionPane.showMessageDialog(null, "No hay un equipo registrado para modificar, primero debe registrar uno.");          
          
          }
            
          break;
          
          //LO ANTERIOR ES EL CONTROLADOR DEL NUMERO 4
          
        case 5:
          
          if(datosEquipo!=null){
          
            JOptionPane.showMessageDialog(null, datosEquipo.toString());          
              
          }
          else{
            JOptionPane.showMessageDialog(null, "No hay un equipo registrado. Primero debe registrarlo.");
          }
          
          break;
          
          //LO ANTERIOR ES EL CONTROLADOR DEL NUMERO 5
          
        case 6:
          
          if(contadorU!=0){
            
            if(datosEquipo!=null){
          
           JOptionPane.showMessageDialog(null, "******Datos del Prestamo******"+"\nIdentificacion: "+identificacion+"\nNombre: "+nombre+"\nTipo de Usuario: "+usuario+"\nPlaca del equipo: "+datosEquipo.getPlaca()+"\nFecha de prestamo: "+fechaP+"\nFecha de devolucion: "+fechaD+"\nObservaciones: "+
                                        datosEquipo.getObservaciones());         
              
          }
          else{
            JOptionPane.showMessageDialog(null, "No hay un equipo o usuario registrado. Primero debe registrarlo.");
          }
          
          }else{
            
            JOptionPane.showMessageDialog(null, "No hay un equipo o usuario registrado. Primero debe registrarlo.");          
          
          }
                   
          break;
          
          //LO ANTERIOR ES EL CONTROLADOR DEL NUMERO 6
          
        case 7:
          
          JOptionPane.showMessageDialog(null, "Se cerrara la aplicacion.");
          
          break;
          
        default:
          
          JOptionPane.showMessageDialog(null, "No se ingreso una opcion valida.");
          
          //LO ANTERIOR ES EL CONTROLADOR DEL NUMERO 7
          
      }//FIN DEL SWITCH PRINCIPAL

    
    
    }while(menuP!=7);



}

}//FIN DE LA CLASE PRESTAMOS