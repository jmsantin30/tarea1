public class Equipo{

private String placa, accesorios, observaciones, tipo, modelo;

//SE INICIAN LOS METODOS CONSTRUCTORES

public Equipo(){}//Metodo constructor que no recibe ningun parametro

public Equipo(String placa, String accesorios, String observaciones, String tipo, String modelo){
  this.placa=placa;
  this.accesorios=accesorios;
  this.observaciones=observaciones;
  this.tipo=tipo;
  this.modelo=modelo;
  }//Metodo constructor que recibe parametros

//FIN DE LOS METODOS CONSTRUCTORES

//SE INICIAN LOS METODOS GET Y SET

public void setPlaca(String placa){
  this.placa=placa;
  }
  
  public String getPlaca(){
  return placa;
  }
  
  public void setAccesorios(String accesorios){
  this.accesorios=accesorios;
  }
  
  public String getAccesorios(){
  return accesorios;
  }
  
  public void setObservaciones(String observaciones){
  this.observaciones=observaciones;
  }
  
  public String getObservaciones(){
  return observaciones;
  }
  
  public void setTipo(String tipo){
  this.tipo=tipo;
  }
  
  public String getTipo(){
  return tipo;
  }
  
  public void setModelo(String Modelo){
  this.modelo=modelo;
  }
  
  public String getModelo(){
  return modelo;
  }
  
  //FIN DE LOS METODOS SET Y GET
  
  //METODO TOSTRING
  
  public String toString(){
  return "***Caracteristicas del equipo***\nPlaca: "+placa+"\nAccesorios: "+accesorios+"\nEstado: "+observaciones+"\nTipo: "+tipo+"\nModelo: "+modelo;
  }
  
  

}//FIN DE LA CLASE EQUIPO